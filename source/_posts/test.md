---
title: 第一章 Java简介
date: 2020-12-01 00:00:00
tags: JAVA
categories: JAVA
keywords: JAVA
description: JAVA 笔记 (一) Java简介
cover: https://cdn.jsdelivr.net/gh/Nightliuguoxing/Pic@main/image/cover/1647089658293java-logo.webp
bilibili_banner: autumn
swiper_index: 1 
copyright: false
package: java
uri: 1
top: false
top_img: 
toc: true
mathjax: false
comments: true
---
Welcome to [Hexo](https://hexo.io/)! This is your very first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).

## Quick Start

### Create a new post

``` bash
$ hexo new "My New Post"
```

More info: [Writing](https://hexo.io/docs/writing.html)

### Run server

``` bash
$ hexo server
```

More info: [Server](https://hexo.io/docs/server.html)

### Generate static files

``` bash
$ hexo generate
```

More info: [Generating](https://hexo.io/docs/generating.html)

### Deploy to remote sites

``` bash
$ hexo deploy
```

More info: [Deployment](https://hexo.io/docs/one-command-deployment.html)
